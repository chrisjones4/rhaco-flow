import freud
import gsd
import gsd.pygsd
import gsd.hoomd
import signac
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components
from scipy.interpolate import InterpolatedUnivariateSpline
from collections import defaultdict
from collections import OrderedDict
from collections import Counter


class System:

    def __init__(self, system, unit_cell_repeats):
        self.system = system
        self.x_repeat = unit_cell_repeats[0]
        self.y_repeat = unit_cell_repeats[1]
        self.z_repeat = unit_cell_repeats[2]
        self.molecule_labels = snap_bond_graph(self.system)
        self.mol_count_dict = Counter(self.molecule_labels)
        self.box = system.configuration.box
        self.surface_atoms = self.surface(self.system)
        self.configuration = self.find_box_configuration(self.system)
        self.top_surface, self.bottom_surface = self.surface_z_values(self.system)
        self.active_sites = self.find_active_sites()

    def molecules(self, stoich_dict):
        # number of atoms in the molecule
        dict_atoms = []
        for key in stoich_dict:
            dict_atoms.extend([key for i in range(stoich_dict[key])])
        dict_atoms.sort()
        # narrow down IDs by length (length of ID == num of atoms in that id)
        molecule_ids = [key for key in self.mol_count_dict if self.mol_count_dict[key] == len(dict_atoms)]
        molecule_ids_keep = []

        for mol_id in molecule_ids:
            atom_indices = np.where(self.molecule_labels == mol_id)[0]
            type_ids = list(self.system.particles.typeid[atom_indices])
            atoms = [self.system.particles.types[typeid] for typeid in type_ids]
            atoms.sort()
            if atoms == dict_atoms:
                molecule_ids_keep.append(mol_id)

        return [Molecule(i, self.molecule_labels) for i in molecule_ids_keep]

    def find_box_configuration(self, system):
        '''
        Determine if the system is set up in either of 2 possible configurations
        Box: The two surfaces are situated at the top and bottom of the system.
            The surfaces are facing eachother.
        Open: The two surfaces are near the center of the box
            The surfaces are facing away from each other

        The results of this function are used when determining the z-value range that is
        considered "on the surface".  Depending on how the system is configured there
        will be slightly different rules.
        '''
        system_ids = [p for p in system.particles.typeid]
        Mo_indices = np.where([i == system.particles.types.index("Mo") for i in system_ids])[0]
        Mo_z_pos = system.particles.position[Mo_indices][::,-1]
        surface_separation = np.max(Mo_z_pos) - np.min(Mo_z_pos)
        if surface_separation > system.configuration.box[2] / 2:
            configuration = "box"
        else:
            configuration = "open"
        return configuration

    def surface(self, system):
        '''
        Determines what the top of each M1 surface looks like.
        If its uncovered, then the surface atoms are oxygens
        If it is covered, then the surface atoms are separate from the M1+Gas types
        The results of this function are used to determine the Z coordinate value
        of the top surface and bottom surface
        '''
        # These atom types will always be present for an M1 system with ethane/O2/He
        standard_types = ['Te', 'Mo', 'V', 'Nb', 'O', 'He', 'opls_135', 'opls_140']
        if all(typ in standard_types for typ in system.particles.types):
            surface_atoms = 'O' # No other atom types found, so no layer above M1
        else: # Other types exist outside of the standard types; these are the layer molecules
            surface_atoms = [typ for typ in system.particles.types if typ not in standard_types]
        return surface_atoms

    def surface_z_values(self, system):
        '''
        Determines the z coordinate value that corresponds with the top of each surface
        Depends on the system's surface atoms and configuration
        '''
        system_ids = [p for p in system.particles.typeid]
        type_indices = np.where(np.isin(system.particles.types, self.surface_atoms) == True)[0]
        atom_indices = np.where(np.isin(system_ids, type_indices) == True)[0]
        atom_z_positions = system.particles.position[atom_indices][::,-1]
        if self.configuration == 'box': # Top surface facing down, bottom surface facing up
            top_surface = np.min((atom_z_positions[atom_z_positions>0]))
            bottom_surface = np.max((atom_z_positions[atom_z_positions<0]))
        elif self.configuration == 'open': # Top surface facing up, bottom surface facing down
            top_surface = np.max(atom_z_positions)
            bottom_surface = np.min(atom_z_positions)
        return top_surface, bottom_surface

    def find_active_sites(self):
        system = self.system
        all_active_species = ['V', 'Mo', 'Nb', 'Te']
        number_active_sites = self.x_repeat * self.y_repeat * self.z_repeat * 8 * 2
        num_surface_active_sites = number_active_sites / (self.z_repeat * 2)
        type_ids = system.particles.typeid
        active_species_idx = [idx for idx, _id in enumerate(type_ids)
                          if system.particles.types[_id] in all_active_species]

        active_site_dict = {} # Contains all 2-bond active sites in the system
        for n in active_species_idx:
            n_count = np.count_nonzero(system.bonds.group == n)
            if n_count == 1:
                bond_info = system.bonds.group[np.where(system.bonds.group == n)[0]]
                bonded_atom = bond_info[0][np.where(bond_info[0] != n)[0]]
                active_site_pos = system.particles.position[n]
                active_site_z = active_site_pos[-1]
                bonded_atom_z = system.particles.position[bonded_atom][0][-1]
                active_site_type = system.particles.types[system.particles.typeid[n]]
                bonded_atom_type = system.particles.types[system.particles.typeid[bonded_atom[0]]]
                
                above = False
                if active_site_z > bonded_atom_z:
                    above = True

                active_site_dict[n] = [active_site_type, bonded_atom[0], above, active_site_pos]
                
        if len(active_site_dict.keys()) != number_active_sites:
            raise ValueError("Double check your values for unit cell repeats")

        active_site_dict_sorted = sorted(active_site_dict.items(), key=lambda x: x[1][-1][-1])
        if self.configuration == 'box':    
            # BOTTOM (top layer of bottom half)
            bottom_start_idx = int((number_active_sites / (2 * self.z_repeat)) - 1)
            bottom_stop_idx = int((number_active_sites / 2))
            bottom_dict = {}
            for tup in active_site_dict_sorted[bottom_start_idx:bottom_stop_idx]:
                if tup[1][2] == True: # Check results of the 'above' variable
                    bottom_dict[tup[0]] = tup[1]
                
            #TOP (bottom layer of top half)
            top_start_idx = int((number_active_sites) / 2) # 400
            top_stop_idx = int((number_active_sites) - (number_active_sites / (2 * self.z_repeat)) + 1)
            top_dict = {}
            for tup in active_site_dict_sorted[top_start_idx:top_stop_idx]:
                if tup[1][2] == False:
                    top_dict[tup[0]] = tup[1]
            
        elif self.configuration == 'open':
            #BOTTOM (top layer of the top half)
            bottom_start_idx = 0
            bottom_stop_idx = int(number_active_sites / (2 * self.z_repeat))
            bottom_dict = {}
            for tup in active_site_dict_sorted[bottom_start_idx:bottom_stop_idx]:
                if tup[1][2] == False: # Check results of the 'above' variable
                    bottom_dict[tup[0]] = tup[1]
            
            #TOP (bottom layer of the bottom half)
            top_dict = {}
            top_start_idx = int((number_active_sites) - (number_active_sites / (2 * self.z_repeat)))
            for tup in active_site_dict_sorted[top_start_idx:]:
                if tup[1][2] == True:
                    top_dict[tup[0]] = tup[1]
        
        final_active_site_dict = {}
        final_active_site_dict.update(bottom_dict)
        final_active_site_dict.update(top_dict)
        return final_active_site_dict

class Molecule:
    def __init__(self, id_num, molecules):
        self.molecule_id = id_num
        self.atom_indices = np.where(molecules == self.molecule_id)[0]
        self.surface_pos = []
        self.surface_frames = []
        self.active_site_types = []
        self.active_site_frames = []
        self.hit_counter = 0

    def atom_types(self, frame):
        type_ids = [frame.particles.typeid[self.atom_indices]]
        atoms = [frame.particles.types[typeid] for typeid in type_ids]
        return atoms

    def center_xyz(self, frame):
        positions = frame.particles.position[self.atom_indices]
        molecule_center = np.mean(positions, axis=0)
        return molecule_center

    def log_molecule(self, frame_num, frame, system, dist_cutoff=0.60):
        '''
        Creates a new list for each new surface interaction
        That list contains positions for all consecutive frames
        that the molecule stays on the surface
        I'm allowing a bit of a buffer, if a molecule is off the surface for a single
        frame between 2 frames where it is on the surface, its all counted as the same surface
        interaction.
        '''
        try:
            last_frame = self.surface_frames[-1][-1]
            if last_frame >= frame_num -1: # -2 is the buffer
                self.surface_frames[-1].append(frame_num)
                self.surface_pos[-1].append(self.center_xyz(frame))
            else: # New interaction, append an empty list, then start appending positions
                self.surface_frames.append([])
                self.surface_pos.append([])
                self.surface_frames[-1].append(frame_num)
                self.surface_pos[-1].append(self.center_xyz(frame))
        except:
            self.surface_frames.append([frame_num])
            self.surface_pos.append([self.center_xyz(frame)])

        active_site = active_site_hit(self.center_xyz(frame), system.active_sites, dist_cutoff)
        if active_site:
            self.hit_counter += 1

    def molecule_msd(self, system_box):
        self.avg_msd = average_mol_msd(self, box=system_box)

    
def snap_bond_graph(snap):
    """
    Given a snapshot from a trajectory return an array
    corresponding to the molecule index of each particle
    Parameters
    ----------
    snap : gsd.hoomd.Snapshot
    Returns
    -------
    numpy array (N_particles,)
    """
    bond_graph = csr_matrix(
        (np.ones(snap.bonds.N), (snap.bonds.group[:,0],snap.bonds.group[:,1])),
        shape=(snap.particles.N,snap.particles.N))
    n_components, labels = connected_components(csgraph=bond_graph, directed=False)
    return labels


def msd(position_list, box, mode='window'):
    positions = np.stack(np.array(position_list)[:, np.newaxis])
    mol_msd = freud.msd.MSD(box, mode)
    mol_msd.compute(positions)
    return mol_msd.msd

def average_system_msd(molecules, system_box, minimum_frames=2,
        minimum_particles=20):
    '''
    '''
    surface_pos_lengths = []
    for mol in molecules:
        if len(mol.surface_pos) > 0: # If = 0; mol never reached the surface
            surface_pos_lengths.extend([len(pos_list) for pos_list in mol.surface_pos])
    surface_pos_lengths.sort() # Must go from smallest to largest
    surface_pos_lengths = [i for i in surface_pos_lengths if i >= minimum_frames]
    surface_pos_lengths_set = list(OrderedDict.fromkeys(surface_pos_lengths))
    msd_results_list = [] # List of arrays
    for mol in molecules:
        for positions in mol.surface_pos:
            if len(positions) >= minimum_frames:
                mol_msd = msd(positions, system_box)
                mol_msd.resize(max(surface_pos_lengths)) # Keep everything the same shape
                msd_results_list.append(mol_msd)

    msd_results_array = np.vstack(msd_results_list) # 2D array
    num_columns = msd_results_array.shape[1]
    
    # Iterate through each column and count how many particles are contributing to that data point
    # If it isn't >= minimum_particles, then zero out the entire column
    for i in range(num_columns):
        if np.count_nonzero(msd_results_array[:, i]) < minimum_particles:
            msd_results_array[:, i] *= 0

    std_devs = np.array([np.std(msd_results_array[:, i]) for i in range(num_columns)])
    # Add all columns up for bin averaging, results in a 1D array
    msd_results = np.array([np.sum(msd_results_array[:, i]) for i in range(num_columns)])

    start_index = 0
    divide_by = float(len(surface_pos_lengths))
    for L in surface_pos_lengths_set:
        if L == max(surface_pos_lengths): # the last iteration, slice to end of msd_results
            num_data_points = len(msd_results[start_index:])
            msd_results[start_index:] /= divide_by
        else: # slice between start_index and L
            num_data_points = len(msd_results[start_index:L])
            msd_results[start_index:L] /= divide_by
        start_index = L
        divide_by -= surface_pos_lengths.count(L)
    cut_index = np.where(msd_results == 0)[0][0]
    return msd_results[:cut_index], std_devs[:cut_index]


def average_mol_msd(molecule, box):
    if len(molecule.surface_pos) == 0:
        return [], []
    lengths = [len(pos_list) for pos_list in molecule.surface_pos]
    lengths.sort() # Must go from smallest to largest
    lengths_set = list(OrderedDict.fromkeys(lengths)) # Remove duplicates

    msd_results = np.zeros(max(lengths), dtype=float)
    for positions in molecule.surface_pos:
        mol_msd = msd(positions, box=box)
        mol_msd.resize(msd_results.shape) # Resize so we can add to msd_results
        msd_results += mol_msd

    standard_deviations = [] # std dev of each bin
    start_index = 0
    divide_by = float(len(molecule.surface_pos))
    for L in lengths_set:
        if L == max(lengths): # Last iteration, slice to the end of msd_results
            num_data_points = len(msd_results[start_index:])
            msd_results[start_index:] /= divide_by
            break
        else: # NOT the last iteration, slice msd_results between start_index & L
            num_data_points = len(msd_results[start_index:L])
            msd_results[start_index:L] /= divide_by
        start_index = L
        divide_by -= lengths.count(L) # .count handles multiple surface instances of same len
    return msd_results


def on_surface(system, molecule, frame, threshold):
    zpos = molecule.center_xyz(frame)[-1]
    onSurface = False

    if system.configuration == "box":
        if (system.top_surface - threshold) < zpos < system.top_surface:
            onSurface = True
        elif system.bottom_surface < zpos < (system.bottom_surface + threshold):
            onSurface = True

    if system.configuration == "open":
        if system.top_surface < zpos < (system.top_surface + threshold):
            onSurface = True
        elif (system.bottom_surface - threshold) < zpos < system.bottom_surface :
            onSurface = True
    return onSurface

def active_site_hit(mol_pos, active_sites, dist_cutoff):
    hit = None
    for key in active_sites:
        site_pos = active_sites[key][-1]
        dist = scipy.spatial.distance.euclidean(mol_pos, site_pos)
        if dist <= dist_cutoff:
            hit = active_sites[key][0] # active site atom type
            break
    return hit

def track_molecules(gsdfile, stoich_dict, unit_cell_repeats, 
                    start_frame=0, surface_threshold=0.34, 
                    active_site_threshold=0.60):
    f = gsd.pygsd.GSDFile(open(gsdfile, "rb"))
    t = gsd.hoomd.HOOMDTrajectory(f)
    system = System(t[0], unit_cell_repeats)
    gas_mols = system.molecules(stoich_dict)

    num_surface_molecules = []
    time_steps = []
    for idx, frame in enumerate(t[start_frame:]):
        surface_gas_mols = [m for m in gas_mols if on_surface(system, m, frame, surface_threshold)]
        num_surface_molecules.append(len(surface_gas_mols))
        time_steps.append(frame.configuration.step)
        for m in surface_gas_mols: # update statuses
            m.log_molecule(idx, frame, system, active_site_threshold)

    for m in gas_mols: # After all frames, calculate MSDs for individual molecules
        m.molecule_msd(system.box)

    return gas_mols, system
