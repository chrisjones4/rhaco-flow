import copy
import sys
import signac
import itertools
from collections import OrderedDict


def get_gen_parameters():
    parameters = OrderedDict()
    # Generate Parameters
    parameters["dimensions"] = ["10x10x2"]
    parameters["template"] = ["M1UnitCell.pdb"]
    parameters["crystal_separation"] = [25.0]
    parameters["reactant_composition"] = ["{'C2H6': 1}"]
    #parameters["reactant_num_mol"] = [250]
    #parameters["reactant_position"] = [None]
    parameters["reactant_density"] = ["0.01" ]
    parameters["forcefield"] = ["M1_oplsaa.xml"]
    parameters["crystal_x"] = ["2.148490"]
    parameters["crystal_y"] = ["2.664721"]
    parameters["crystal_z"] = ["0.400321"]
    parameters["job_type"] = ["parent"]
    parameters["z_reactor_size"] = ["15.0"]
    parameters["surface_file"] = ["10x10x2-benzene-06r.mol2"]
    return list(parameters.keys()), list(itertools.product(*parameters.values()))


def get_sim_parameters():
    parameters = OrderedDict()
    # Simulate Parameters
    parameters["temperature"] = [433, 533, 633, 733]
    parameters["run_time"] = [1E8]
    #parameters["omit_lj"] = ["Ag-Ag"]
    parameters["energy_scale_unit"] = ["1.0"]
    parameters["job_type"] = ["child"]
    return list(parameters.keys()), list(itertools.product(*parameters.values()))


if __name__ == "__main__":
    project = signac.init_project("M1-Testing")
    gen_param_names, gen_param_combinations = get_gen_parameters()
    sim_param_names, sim_param_combinations = get_sim_parameters()
    # Create the generate jobs
    for gen_params in gen_param_combinations:
        parent_statepoint = dict(zip(gen_param_names, gen_params))
        parent_job = project.open_job(parent_statepoint)
        parent_job.init()
        for sim_params in sim_param_combinations:
            child_statepoint = copy.deepcopy(parent_statepoint)
            child_statepoint.update(dict(zip(sim_param_names, sim_params)))
            child_statepoint["parent_statepoint"] = parent_job.statepoint()
            project.open_job(child_statepoint).init()
    project.write_statepoints()
